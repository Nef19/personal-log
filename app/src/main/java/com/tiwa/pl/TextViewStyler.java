package com.tiwa.pl;

import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.widget.TextView;

public class TextViewStyler {

    public void styleTextView(TextView mTextView) {
        String text = mTextView.getText().toString();
        if (!text.contains("#") || text.length() < 5) {
            return;
        }
        mTextView.setText(mTextView.getText(), TextView.BufferType.SPANNABLE);
        changeLineinView_TITLESTYLE(mTextView, "# ", Color.BLACK, 1.5f);
        changeLineinView(mTextView, "\n# ", Color.BLACK, 1.5f, true);
        changeLineinView(mTextView, "\n## ", Color.BLACK, 1.2f, true);
        changeLineinView(mTextView, "\n- ", Color.BLACK, 1.0f, false);
    }

    private void changeLineinView(TextView tv, String target, int colour, float size, boolean boldItalic) {
        String vString = tv.getText().toString();
        int startSpan = 0, endSpan = 0;
        Spannable spanRange = (Spannable) tv.getText();
        while (true) {
            startSpan = vString.indexOf(target, endSpan - 1);
            endSpan = vString.indexOf("\n", startSpan + 1);
            ForegroundColorSpan foreColour = new ForegroundColorSpan(colour);
            if ((startSpan < 0) || (endSpan < 0))
                break;
            if (endSpan > startSpan) {
                spanRange.setSpan(foreColour, startSpan, endSpan, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spanRange.setSpan(new RelativeSizeSpan(size), startSpan, endSpan, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (boldItalic)
                    spanRange.setSpan(new StyleSpan(Typeface.BOLD), startSpan, endSpan, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        tv.setText(spanRange);
    }

    private void changeLineinView_TITLESTYLE(TextView tv, String target, int colour, float size) {
        String vString = tv.getText().toString();
        int startSpan = 0, endSpan = 0;
        Spannable spanRange = (Spannable) tv.getText();
        startSpan = vString.substring(0, target.length()).indexOf(target, endSpan - 1);
        endSpan = vString.indexOf("\n", startSpan + 1);
        ForegroundColorSpan foreColour = new ForegroundColorSpan(colour);
        if (!(startSpan < 0)) {
            if (endSpan > startSpan) {
                spanRange.setSpan(foreColour, startSpan, endSpan, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spanRange.setSpan(new RelativeSizeSpan(size), startSpan, endSpan, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spanRange.setSpan(new StyleSpan(Typeface.BOLD), startSpan, endSpan, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            }
        }
        tv.setText(spanRange);
    }
}
